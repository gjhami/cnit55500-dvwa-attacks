#!/usr/bin/sh
SESSID=i40parlnj1v7s2e7g2mt9t5nqn

for FILE in ./attack-servers/*; do
	bash "${FILE}" &
done

for FILE in ./attack-scripts/*; do
	python3 "${FILE}" "${SESSID}"
done
